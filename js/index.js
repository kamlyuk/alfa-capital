$(function() {
	
	/* AJAX setup */
	
	$.ajaxSetup({
		type: 'POST',
		processData: true,
		dataType: 'json',
		async: true,
		contentType: 'application/x-www-form-urlencoded; charset=UTF-8'
	});
	
	/* AJAX setup END */
	
	/* Всплывающие окна setup */
	
	$.tooltipster.setDefaults({
		side: ['right', 'bottom', 'top', 'left'],
		minWidth: 160,
		trigger: 'custom',
		triggerClose: {
			click: true,
			tap: true,
			originClick: true
		},
		trackOrigin: true,
		trackerInterval: 10,
		contentAsHTML: true,
		functionAfter: function(instance) {
			instance.destroy();
		}
	});
	
	/* Всплывающие окна setup END */
	
	/* Всплывающие окна - подсказки */
	
	$(window).off('resize.tooltips').on('resize.tooltips', function() {
		$('.js-tooltip').each(function(){
			if ($(this).hasClass('tooltipstered'))
				$(this).tooltipster('destroy');
			
			$(this).tooltipster({
				distance: -9,
				maxWidth: 359,
				content: $(this).attr('data-tooltip'),
				side: ['top', 'bottom', 'right', 'left'],
				contentCloning: true,
				trigger: ($(window).width() >= 768) ? 'hover' : 'click',
				theme: 'tooltipster-default',
				interactive: true,
				functionAfter: function() {}
			});
		});
	});
	
	$(window).trigger('resize');
	
	/* Всплывающие окна - подсказки END */
	
	// Открытие модального окна
	function modalOpen(dataModal) {
		var modal = $('.js-modal[data-modal=' + dataModal + ']').first().appendTo('.js-modals');

        modal.width();

		modal.addClass('active');
	}

	$('.js-modal-open').on('click', function () {
		modalOpen($(this).attr('data-modal'));
	});

    // Закрытие модального окна
    function modalClose(dataModal) {
        var modal = $('.js-modal[data-modal=' + dataModal + ']').first().removeClass('active');
    }

    $('.js-modal-close').on('click', function () {
    	modalClose($(this).closest('.js-modal').attr('data-modal'));
    });

    /* Модальные окна END */
	
	/* Прилепание шапки в мобильной версии */
	
	$('.js-header-scroll').each(function(){
		var header = $(this);
		var stickyHeader = $('<div class="header__sticky"></div>').prependTo('body');
		$(this).find('.js-header-scroll-element').clone(true).appendTo(stickyHeader);
		
		$(window).off('scroll.sticky').on('scroll.sticky', function() {
			if($(this).scrollTop() >= header.innerHeight())
				stickyHeader.addClass('sticked');
			else
				stickyHeader.removeClass('sticked');
		});
		
		// Прилепание шапки в desktop-версии
		if (!$(this).hasClass('js-header-scroll-no-desktop'))
			header.clone(true).addClass('header__wrapper-sticky').prependTo('body');
	});
	
	/* Прилепание шапки в мобильной версии END */
	
	/* SMS */
	
	// Получить SMS-код
	$('.js-sms-get').first().off('click.sms-get').on('click.sms-get', function() {
		
		$(this).addClass('hidden');
		
		var tooptip = function(text, type) {
			if (text !== undefined)
				
				var theme = 'tooltipster-default';
			if (type !== undefined)
				var theme = 'tooltipster-' + type;
			
			$('.js-sms-get').removeClass('hidden').tooltipster({
				content: text,
				theme: 'tooltipster-error',
			}).tooltipster('open');
		}
		
		$.ajax({
			url: $(this).attr('data-url'),
			data: {token: $('meta[name=token]').attr('content')},
			success: function (answer)
			{
				if (answer && answer.status && answer.status === 'success') {
					$('.js-sms').first().removeClass('hidden');
					
					$('.js-sms-input').off('input.sms-length change.sms-length').on('input.sms-length change.sms-length', function() {
						if ($(this).hasClass('tooltipstered'))
							$(this).tooltipster('close');
						
						if ($(this).val().length === parseInt($(this).attr('data-length')))
							$('.js-sms-send').removeClass('disabled')
						else
							$('.js-sms-send').addClass('disabled')
					}).trigger('change').trigger('focus');
					
					setTimeout(function(){
						$('.js-sms-resend').first().removeClass('hidden');
					}, parseInt($('.js-sms-timer').attr('data-timer')) * 1000);
				}
				else if (answer.tooltip)
					if (answer.status === 'error')
						tooptip(answer.tooltip, 'error');
					else
						tooptip(answer.tooltip);
					else
						tooptip('Отправить SMS не получилось.<br> Пожалуйста, попробуйте ещё раз.', 'error');
				},
				error: function (answer)
				{
					tooptip('Отправить SMS не получилось.<br> Пожалуйста, попробуйте ещё раз.', 'error');
				}
			});
	});

	$('.js-result-sms-get').first().off('click.sms-get').on('click.sms-get', function() {
		$(this).hide();
		
		var tooptip = function(text, type) {
			if (text !== undefined)
				
				var theme = 'tooltipster-default';
			if (type !== undefined)
				var theme = 'tooltipster-' + type;
			
			$('.js-sms-get').removeClass('hidden').tooltipster({
				content: text,
				theme: 'tooltipster-error',
			}).tooltipster('open');
		}
		
		$.ajax({
			url: $(this).attr('data-url'),
			data: {token: $('meta[name=token]').attr('content')},
			success: function (answer)
			{
				if (answer && answer.status && answer.status === 'success') {
					$('.js-sms').fadeIn()
					
					$('.js-sms-input').off('input.sms-length change.sms-length').on('input.sms-length change.sms-length', function() {
						if ($(this).hasClass('tooltipstered'))
							$(this).tooltipster('close');
						
						if ($(this).val().length === parseInt($(this).attr('data-length')))
							$('.js-sms-send').removeClass('disabled')
						else
							$('.js-sms-send').addClass('disabled')
					}).trigger('change').trigger('focus');
					
					setTimeout(function(){
						$('.js-sms-resend').first().removeClass('hidden');
					}, parseInt($('.js-sms-timer').attr('data-timer')) * 1000);
				}
				else if (answer.tooltip)
					if (answer.status === 'error')
						tooptip(answer.tooltip, 'error');
					else
						tooptip(answer.tooltip);
					else
						tooptip('Отправить SMS не получилось.<br> Пожалуйста, попробуйте ещё раз.', 'error');
			},
			error: function (answer)
			{
				tooptip('Отправить SMS не получилось.<br> Пожалуйста, попробуйте ещё раз.', 'error');
			}
		});
	});
	
	// Прислать SMS повторно
	$('.js-sms-resend').first().off('click.sms-get').on('click.sms-get', function() {
		
		$(this).addClass('hidden');
		
		var tooptip = function(text, type) {
			if (text !== undefined)
				
				var theme = 'tooltipster-default';
			if (type !== undefined)
				var theme = 'tooltipster-' + type;
			
			$('.js-sms-resend').removeClass('hidden').tooltipster({
				content: text,
				theme: 'tooltipster-error',
			}).tooltipster('open');
		}
		
		var countDown = function(dateTimerStop) {
			var now = new Date();
			
			if (now.getTime() >= dateTimerStop.getTime()) {
				$('.js-sms-timer').first().addClass('hidden').text(0);
				$('.js-sms-resend').first().removeClass('hidden');
			}
			else {
				var timeLeft = (dateTimerStop.getTime() - now.getTime()) / 1000;
				timeLeft = Math.round(timeLeft);
				$('.js-sms-timer').first().text(timeLeft);
				setTimeout(function() {countDown(dateTimerStop);}, 1000);
			}
		}
		
		$.ajax({
			url: $(this).attr('data-url'),
			data: {token: $('meta[name=token]').attr('content')},
			success: function (answer)
			{
				if (answer && answer.status && answer.status === 'success') {
					
					$('.js-sms-timer').first().removeClass('hidden');
					
					var dateTimerStop = new Date();
					dateTimerStop.setSeconds(dateTimerStop.getSeconds() + parseInt($('.js-sms-timer').attr('data-timer')));
					countDown(dateTimerStop);
					$('.js-sms-input').first().trigger('focus');
				}
				else if (answer.tooltip)
					if (answer.status === 'error')
						tooptip(answer.tooltip, 'error');
					else
						tooptip(answer.tooltip);
					else
						tooptip('Прислать SMS повторно не получилось.<br> Пожалуйста, попробуйте ещё раз.', 'error');
				},
				error: function (answer)
				{
					tooptip('Прислать SMS повторно не получилось.<br> Пожалуйста, попробуйте ещё раз.', 'error');
				}
			});
	});
	
	// Отправить введённый SMS-код
	$('.js-sms-send').first().each(function() {
		
		var sendSms = function() {
			
			if($('.js-sms-send').hasClass('disabled'))
				return false;
			
			modalOpen('loading');
			
			var tooptip = function(text, type) {
				if (text !== undefined)
					
					var theme = 'tooltipster-default';
				if (type !== undefined)
					var theme = 'tooltipster-' + type;
				
				$('.js-sms-send').tooltipster({
					content: text,
					theme: 'tooltipster-error',
				}).tooltipster('open');
				
				modalClose('loading');
			}
			
			$.ajax({
				url: $('.js-sms-send').attr('data-url'),
				data: {token: $('meta[name=token]').attr('content'), code: $('.js-sms-input').val()},
				success: function (answer)
				{
					if (answer && answer.status && answer.status === 'success') {
						if (answer.redirect !== undefined)
							window.location = answer.redirect;
					}
					else if (answer && answer.tooltip)
						if (answer.status === 'error')
							tooptip(answer.tooltip, 'error');
						else
							tooptip(answer.tooltip);
						else
							tooptip('Отправить SMS-код не получилось.<br> Пожалуйста, попробуйте ещё раз.', 'error');
					},
					error: function (answer)
					{
						tooptip('Отправить SMS-код не получилось.<br> Пожалуйста, попробуйте ещё раз.', 'error');
					}
				});
		}
		
		$('.js-sms-send').first().off('click.sms-send').on('click.sms-send', function() {
			sendSms();
		});
		
		$('.js-sms-input').first().off('keydown.sms-send').on('keydown.sms-send', function (e) {
			if (e.which === 13) {
				sendSms();
			}
		});
		
	});
	$('.js-result-sms-send').first().each(function() {
		
		var sendSms = function() {
			
			if($('.js-result-sms-send').hasClass('disabled'))
				return false;
			
			var tooptip = function(text, type) {
				if (text !== undefined)
					
					var theme = 'tooltipster-default';
				if (type !== undefined)
					var theme = 'tooltipster-' + type;
				
				$('.js-result-sms-send').tooltipster({
					content: text,
					theme: 'tooltipster-error',
				}).tooltipster('open');
				
				modalClose('loading');
			}
			
			$.ajax({
				url: $('.js-result-sms-send').attr('data-url'),
				data: {token: $('meta[name=token]').attr('content'), code: $('.js-sms-input').val()},
				success: function (answer)
				{
					if (answer && answer.status && answer.status === 'success') {
						$('.result__sms').hide()
						$('.result__done').fadeIn()

					}
					else if (answer && answer.tooltip)
						if (answer.status === 'error')
							tooptip(answer.tooltip, 'error');
						else
							tooptip(answer.tooltip);
						else
							tooptip('Отправить SMS-код не получилось.<br> Пожалуйста, попробуйте ещё раз.', 'error');
					},
					error: function (answer)
					{
						tooptip('Отправить SMS-код не получилось.<br> Пожалуйста, попробуйте ещё раз.', 'error');
					}
				});
		}
		
		$('.js-result-sms-send').first().off('click.sms-send').on('click.sms-send', function() {
			sendSms();
		});
		
		$('.js-sms-input').first().off('keydown.sms-send').on('keydown.sms-send', function (e) {
			if (e.which === 13) {
				sendSms();
			}
		});
		
	});
	
	
	/* SMS END */
	
	/* Input placeholder */
	
	$('.js-placeholder-label').each(function() {
		
		// Focus
		$(this).find('.js-placeholder-input').off('focus.placeholder').on('focus.placeholder', function() {
			var placeholder = $(this).closest('.js-placeholder-label').find('.js-placeholder');
			placeholder.addClass('active');
			placeholder.addClass('focus');
		});
		
		// Blur
		$(this).find('.js-placeholder-input').off('blur.placeholder change.placeholder').on('blur.placeholder change.placeholder', function() {
			var placeholder = $(this).closest('.js-placeholder-label').find('.js-placeholder');
			
			// Если заполнено, оаствить класс "active"
			if ($(this).val().length === 0)
				placeholder.removeClass('active');
			else
				placeholder.addClass('active');
			placeholder.removeClass('focus');
		});
		
	});
	
	/* Input placeholder END */
	
	/* Questions */
	
	$('.js-questions').each(function() {
		
		/* Рекурсивная функция анимации элемента */
		
		var elementsForAnimation = {}; // Глобальная переменная элементов для анимации
		var currentNumber = 0; // Глобальная переменная текущего номера элемента для анимации
		
		var animateElement = function() {
			
			elementsForAnimation.eq(currentNumber).addClass('animated');
			
			if (currentNumber <= elementsForAnimation.length)
				setTimeout(function() {
					currentNumber += 1;
					animateElement();
				}, 75);
		}
		
		/* Рекурсивная функция анимации элемента END */
		
		/* Функция корректировки высоты блока с вопросом */
		
		var correctHeight = function(options) {
			if (typeof options === 'undefined')
				pagesHeight = $('.js-questions-page.active').innerHeight();
			else if (typeof options.page !== 'undefined')
				pagesHeight = $('.js-questions-page[data-page='+options.page+']').innerHeight();
			else if (typeof options.height !== 'undefined')
				pagesHeight = options.height;
			
			
			$('.js-questions-pages').css('height', pagesHeight)
		}
		
		$(window).off('resize.correctHeight').on('resize.correctHeight', function () {
			correctHeight();
			setTimeout(function() {correctHeight()}, 500);
		});
		
		/* Функция корректировки высоты блока с вопросом END */
		
		/* Функция перехода к странице */
		var goToPage = function(page, done) {
			
			var navItems = $('.js-questions-item');
			
			// Ничего не делать, если это попытка перехода на текущую страницу
			if (navItems.filter('.active-1').attr('data-item') === page)
				return false;
			
			// Добавление класса done предыдущему элементу в навигации
			if(done === 'done')
				navItems.filter('.active-1').addClass('done');
			
			// Если не 1-ая страница, то добавление класса, увеличивающего высоту основных блоков
			if(page != 1) {
				$('.js-questions').addClass('tall');
				$('.js-questions-pages').addClass('tall');
			}
			else {
				$('.js-questions').removeClass('tall');
				$('.js-questions-pages').removeClass('tall');
			}
			
			// Изменение высоты точки
			var steps = navItems.filter(':not(.fake)').length;
			var oneStep = 100/steps;
			$('.js-questions-scroll-point').css('top', oneStep * page - oneStep +'%');
			
			// Добавление соответствующих классов соседним навигационным кнопкам
			navItems.removeClass('active-1 active-2 active-3 active-4 active-5')
			navItems.filter('[data-item='+page+']').addClass('active active-1');
			var navIndex = navItems.filter('[data-item='+page+']').index();
			navItems.filter(':eq('+(navIndex-1)+'), :eq('+(navIndex+1)+')').addClass('active-2');
			navItems.filter(':eq('+(navIndex-2)+'), :eq('+(navIndex+2)+')').addClass('active-3');
			navItems.filter(':eq('+(navIndex-3)+'), :eq('+(navIndex+3)+')').addClass('active-4');
			navItems.filter(':eq('+(navIndex-4)+'), :eq('+(navIndex+4)+')').addClass('active-5');
			
			// Отображени/скрытие кнопок назад и вперёд
			if(navItems.filter(':eq('+(navIndex-1)+')').hasClass('active'))
				$('.js-questions-prev').addClass('visible');
			else
				$('.js-questions-prev').removeClass('visible');
			if(navItems.filter(':eq('+(navIndex+1)+')').hasClass('active'))
				$('.js-questions-next').addClass('visible');
			else
				$('.js-questions-next').removeClass('visible');
			
			// Направление анимации исчезновения предыдущей страницы
			$('.js-questions-page').removeClass('animate-up animate-down');
			var indexPrev = $('.js-questions-page.active').index();
			var indexNext = $('.js-questions-page[data-page='+page+']').index();
			if(indexNext > indexPrev)
				$('.js-questions-page.active').addClass('animate-up');
			else
				$('.js-questions-page.active').addClass('animate-down');
			
			// Переключение страницы
			$('.js-questions-page').removeClass('active');
			$('.js-questions-page[data-page='+page+']').addClass('active');
			correctHeight({page: page});
			$('html, body').animate({scrollTop: 0}, 300);
			
			// Анимация поэлементного появления
			elementsForAnimation = $('.js-questions-animate[data-animate='+page+'], .js-questions-page[data-page='+page+'] .js-questions-animate');
			currentNumber = 0;
			$('.js-questions-animate:not([data-animate-once])').removeClass('animated');
			animateElement();
		}
		
		/* Функция перехода к странице END */
		
		/* Кнопки перехода */
		
		$('.js-questions-go').off('click.questions-go').on('click.questions-go', function() {
			
			// Проверка наличия класса у кнопки, содержащегося в атрибуте "data-go-check"
			if($(this).filter('[data-go-check]').length > 0)
				if(!$(this).hasClass($(this).attr('data-go-check')))
					return false;
				
			var timeout = 0; // Задержка, перед нажатием кнопки
			
			// Есть есть атрибут "data-timeout", то установить задержку из него
			if ($(this).filter('[data-timeout]'))
				timeout = $(this).attr('data-timeout')
			
			var that = $(this)
			
			setTimeout(function() {
				
				// Если есть атрибут "data-done", то текущей активной кнопке в навигации добавить класс "done"
				if(that.filter('[data-done]').length > 0)
					goToPage(that.attr('data-go'), 'done');
				else
					goToPage(that.attr('data-go'));
				
			}, timeout, that);
		});
		
		/* Кнопки перехода END */
		
		/* Листание назад и вперёд */
		
		var goNear = function (direction, that) {
			
			if (direction === 'prev')
				var side = -1;
			else if (direction === 'next')
				var side = 1;
			
			// Проверка на наличие класса "visible"
			if (!that.hasClass('visible'))
				return false;
			
			var index = $('.js-questions-item.active-1').index();
			var prev = $('.js-questions-item').eq(index + side);
			if(prev.attr('data-item').length > 0)
				goToPage(prev.attr('data-item'));
		}

		/* Листание назад и вперёд END */
		
		/* Кнопки назад и вперёд */
		
		$('.js-questions-prev').off('click.questions-prev').on('click.questions-prev', function() {
			goNear('prev', $(this));
		});
		
		$('.js-questions-next').off('click.questions-next').on('click.questions-next', function() {
			goNear('next', $(this));
		});
		
		/* Кнопки назад и вперёд END */
		
		/* Листание свайпом */
		
		$('.js-swipe').each(function() {
			$(this).swipe({
				swipe: function(event, direction) {
					if (direction === 'right') {
						goNear('prev', $('.js-questions-prev').first());
					} else if (direction === 'left') {
						goNear('next', $('.js-questions-next').first());
					}
				}
			});
			
			var pages = $(this);
			var isMobile = false;
			pages.swipe("disable");
			
			$(window).off('resize.swipe').on('resize.swipe', function () {
				
				if (isMobile === false && $(this).innerWidth() < 960) {
					pages.swipe("enable");
					isMobile = true;
				} else if (isMobile === true && $(this).innerWidth() >= 960) {
					pages.swipe("disable");
					isMobile = false;
				}
			});
			
			$(window).trigger('resize');
		});
		
		/* Листание свайпом END */
		
		/* Листание колёсиком мыши назад и вперёд */
		
		$('.js-questions-wheel').first().off('mousewheel.questions').on('mousewheel.questions', function(event) {
			if (event.deltaY === 1)
				goNear('prev', $('.js-questions-prev'));
			else
				goNear('next', $('.js-questions-next'));
			return false;
		});
		
		/* Листание колёсиком мыши назад и вперёд END */
		
		/* Инверсия порядка элементов для мобильной версии */
		
		$('.js-inversion').each(function() {
			var wrapper = $(this);
			var isMobile = false;
			var elements = $(this).children();
			
			$(window).off('resize.inversion').on('resize.inversion', function () {
				
				if (isMobile === false && $(this).innerWidth() < 960) {
					wrapper.append(elements.get().reverse());
					isMobile = true;
				} else if (isMobile === true && $(this).innerWidth() >= 960) {
					wrapper.append(elements.get());
					isMobile = false;
				}
			});
			
			$(window).trigger('resize');
		});
		
		/* Инверсия порядка элементов для мобильной версии END */
		
		/* Range на 5 вопросе */
		
		jcf.replace('.js-questions-briefcase-range', 'Range', {
			snapRadius: 15,// Расстояние прилипания к делителям
			orientation: 'vertical' // Ориентация
		});
		
		var briefcaseInputHidden = $('.js-questions-briefcase-hidden').first(); // Скрытый input
		var briefcaseImgFill = $('.js-questions-briefcase-img-fill').first(); // Изображение заполнения
		var briefcaseImgLine = $('.js-questions-briefcase-img-line').first(); // Линия около стрелки
		var briefcaseLevels = $('.js-questions-briefcase-level'); // Подписи на отметках
		var briefcaseBtn= $('.js-questions-briefcase-btn').first(); // Кнопка "Продолжить"
		
		// При изменении range
		$('.js-questions-briefcase-range').off('input.questions-range change.questions-range').on('input.questions-range change.questions-range', function() {
			
			var value = $(this).val();
			
			// Заполнить изображение и поднять линию
			briefcaseImgFill.css('height', value+'%');
			briefcaseImgLine.css('bottom', value+'%');
			
			// Выделить цифру около процента, если range равен ей (атрибуту "data-level" у цифры)
			briefcaseLevels.removeClass('active');
			briefcaseLevels.filter('[data-level="'+value+'"]').addClass('active');
			
			// Убрать плавное перемещение при нажатии
			$(this).closest('.jcf-range').find('.jcf-range-handle').removeClass('animated');
			briefcaseImgFill.removeClass('animated');
			briefcaseImgLine.removeClass('animated');
			
		}).off('change.questions-range-2').on('change.questions-range-2', function() {
			
			var currentBtn = $(this);
			var value = $(this).val();
			
			// Активировать кнопку "Продолжить", если значение равно одной из отметок или подтянуть значение до ближайшего
			var checked = false
			briefcaseLevels.each(function() {
				if (checked === true)
					return false;
				
				if ($(this).attr('data-level') === value)
					checked = true;
			});
			if (checked === true)
				briefcaseBtn.addClass('active');
			else {
				var min = null;
				var minIndex = null;
				briefcaseLevels.each(function() {
					var diff = parseInt(value) - parseInt($(this).attr('data-level'));
					if (diff < 0)
						diff = -diff;
					
					if (min === null) {
						min = diff;
					}
					else if (diff < min) {
						min = diff;
						minIndex = $(this).index();
					}
				});
				$(this).val(briefcaseLevels.eq(minIndex).attr('data-level'));
				$(this).trigger('change');
				var bottom = $(this).closest('.jcf-range').find('.jcf-range-mark[data-mark-value='+$(this).val()+']').css('bottom');
				$(this).closest('.jcf-range').find('.jcf-range-handle').css('bottom', bottom);
			}
			
			// Для плавного перемещения к ближайшей отметке
			$(this).closest('.jcf-range').find('.jcf-range-handle').addClass('animated');
			briefcaseImgFill.addClass('animated');
			briefcaseImgLine.addClass('animated');
			
			setTimeout(function() {
				var scrollHeight = currentBtn.closest('.jcf-range').innerHeight();
				$('html, body').animate({scrollTop: $(window).scrollTop() + scrollHeight}, 300);
			}, 450);
			
			// Заполнение скрытого input
			if ($(this).val() == 0)
				briefcaseInputHidden.val(4);
			else if ($(this).val() == 30)
				briefcaseInputHidden.val(3);
			else if ($(this).val() == 70)
				briefcaseInputHidden.val(2);
			else if ($(this).val() == 100)
				briefcaseInputHidden.val(1);
		});

		/* Range на 5 вопросе END */
		
		/* Выбор нескольких radio для активации кнопки на 6 вопросе */
		
		$('.js-questions-check-several').off('click.questions-check-several').on('click.questions-check-several', function() {
			
			var currentBtn = $(this);
			var allRadio = $('.js-questions-check-several[data-check-several='+$(this).attr('data-check-several')+']');
			var radioNames = {};
			
			// формируем массив из всех "name". Если выбран один из radio у определённого radio, то ставим ему true, иначе false
			allRadio.each(function(){
				if ($(this).is(':checked'))
					radioNames[$(this).attr('name')] = true;
				else if (typeof radioNames[$(this).attr('name')] === 'undefined')
					radioNames[$(this).attr('name')] = false;
			});
			
			// Проверяем, чтобы в каждом "name" был выбран radio
			var activeTarget = true;
			$.each(radioNames, function(index, value) {
				if (value === false)
					activeTarget = false;
			});
			
			// Если выбраны все "name", то активируем кнопку, указанную в атрибуте атрибуту "data-check-several"
			if (activeTarget === true)
				$('.js-questions-check-several-target[data-check-several-target='+$(this).attr('data-check-several')+']').addClass('active');
			else
				$('.js-questions-check-several-target[data-check-several-target='+$(this).attr('data-check-several')+']').removeClass('active');
			
			// Скролл на высоту отвеченного подвопроса
			setTimeout(function() {
				var scrollHeight = currentBtn.closest('.js-questions-scrolling-element').innerHeight();
				$('html, body').animate({scrollTop: $(window).scrollTop() + scrollHeight}, 300);
			}, 450);
		});
		
		/* Выбор нескольких radio для активации кнопки на 6 вопросе END*/
		
		/* Range на 8 вопросе */
		
		jcf.replace('.js-questions-invest-range', 'Range', {
			snapRadius: 1,// Расстояние прилипания к делителям
			orientation: 'vertical' // Ориентация
		});
		
		var investInputHidden = $('.js-questions-invest-hidden').first(); // Скрытый input
		var investImgFill = $('.js-questions-invest-img-fill').first(); // Изображение заполнения
		var investImgLine = $('.js-questions-invest-img-line').first(); // Линия около стрелки
		var investLevels = $('.js-questions-invest-level'); // Подписи на отметках
		var investHandle = $('.js-questions-invest-range + span .jcf-range-handle').first(); // Подпись на отметках
		var investParagraphs = $('.js-questions-invest-paragraph'); // Тексты с процентами
		var investBtn = $('.js-questions-invest-btn').first(); // Кнопка "Определить профиль"
		
		// Добавить текст в ползунок
		investParagraphs.clone(true).appendTo(investHandle);
		
		var percentThis = $('.js-questions-invest-this'); // Процент инвестирование в нашу УК
		var percentOther = $('.js-questions-invest-other'); // Процент инвестирования в другие места
		
		// При изменении range
		$('.js-questions-invest-range').off('input.questions-range change.questions-range').on('input.questions-range change.questions-range', function() {
			
			var value = $(this).val();
			
			// Заполнить изображение и поднять линию
			investImgFill.css('height', value+'%');
			investImgLine.css('bottom', value+'%');
			
			// Заполнить проценты инвестирования в нашу УК и в иные места, округлив до целого числа
			var min = null;
			var minIndex = null;
			investLevels.each(function() {
				var diff = parseInt(value) - parseInt($(this).attr('data-level'));
				if (diff < 0)
					diff = -diff;
				
				if (min === null) {
					min = diff;
				}
				else if (diff < min) {
					min = diff;
					minIndex = $(this).index();
				}
			});
			percentThis.text(parseInt($(this).val()));
			percentOther.text(100 - parseInt($(this).val()));
			if ($(this).val() < 0) {
				percentThis.text(0);
				percentOther.text(100);
			}
			
			// Спрятать процент 0
			if (value < 15)
				investLevels.filter('[data-level="0"]').addClass('hidden');
			else
				investLevels.filter('[data-level="0"]').removeClass('hidden');
			
			// Спрятать процент 100
			if (value > 85)
				investLevels.filter('[data-level="100"]').addClass('hidden');
			else
				investLevels.filter('[data-level="100"]').removeClass('hidden');
			
			// Показать текст около ползунка
			investHandle.addClass('visible');
			
			// Убрать плавное перемещение при нажатии
			$(this).closest('.jcf-range').find('.jcf-range-handle').removeClass('animated');
			investImgFill.removeClass('animated');
			investImgLine.removeClass('animated');
			
		}).off('change.questions-range-2').on('change.questions-range-2', function() {
			
			var currentBtn = $(this);
			var value = $(this).val();
			
			// Активировать кнопку "Определить профиль", если значение больше 0 или подтянуть значение до 0
			var checked = false
			
			if (value >= 0)
				checked = true;
				
			if (checked === true)
				investBtn.removeClass('disabled');
			else {
				var min = null;
				var minIndex = null;
				investLevels.each(function() {
					var diff = parseInt(value) - parseInt($(this).attr('data-level'));
					if (diff < 0)
						diff = -diff;
					
					if (min === null) {
						min = diff;
					}
					else if (diff < min) {
						min = diff;
						minIndex = $(this).index();
					}
				});
				$(this).val(0);
				$(this).trigger('change');
				var bottom = $(this).closest('.jcf-range').find('.jcf-range-mark[data-mark-value=0]').css('bottom');
				$(this).closest('.jcf-range').find('.jcf-range-handle').css('bottom', bottom);
			}
			
			// Для плавного перемещения к ближайшей отметке
			$(this).closest('.jcf-range').find('.jcf-range-handle').addClass('animated');
			investImgFill.addClass('animated');
			investImgLine.addClass('animated');
			
			setTimeout(function() {
				var scrollHeight = currentBtn.closest('.jcf-range').innerHeight();
				$('html, body').animate({scrollTop: $(window).scrollTop() + scrollHeight}, 300);
			}, 300);
			
			// Заполнение скрытого input
			if (value <= 10)
				investInputHidden.val(4);
			else if (value <= 20)
				investInputHidden.val(3);
			else if (value <= 50)
				investInputHidden.val(2);
			else
				investInputHidden.val(1);
		});
		

		/* Range на 8 вопросе END */
		
		/* Отправка формы 2-го шага */
		
		$('.js-questions-identify').first().each(function() {
			
			var sendForm = function() {
				
				// Проверяем отсутствие класса "disabled"
				if($('.js-questions-identify').first().hasClass('disabled'))
					return false;
				
				modalOpen('loading');
				
				var tooptip = function(text, type) {
					if (text == undefined)
						return false;
					
					var theme = 'tooltipster-default';
					if (type !== undefined)
						var theme = 'tooltipster-' + type;
					
					$('.js-questions-identify').tooltipster({
						side: ['left', 'bottom', 'top', 'right'],
						content: text,
						theme: theme,
					}).tooltipster('open');
					
					modalClose('loading');
				}
				
				$.ajax({
					url: $('.js-questions-form').attr('data-url'),
					data: $('.js-questions-form').serialize(),
					success: function (answer) {
						if (answer && answer.status && answer.status === 'success') {
							if (answer.redirect !== undefined)
								window.location = answer.redirect;
						}
						else if (answer && answer.tooltip)
							if (answer.status === 'error')
								tooptip(answer.tooltip, 'error');
							else
								tooptip(answer.tooltip);
							else
								tooptip('Отправить ответы не получилось.<br> Пожалуйста, попробуйте ещё раз.', 'error');
						},
						error: function (answer) {
							tooptip('Отправить ответы не получилось.<br> Пожалуйста, попробуйте ещё раз.', 'error');
						}
					});
			}
			
			$('.js-questions-identify').off('click.questions-identify').on('click.questions-identify', function() {
				sendForm();
			});
			
		});
		
		/* Отправка формы 2-го шага END */
		
		goToPage(1);


	});

/* Questions END */
});